def call() {
        pipeline {
                agent any
                stages {
                        stage('Gradle Build'){
                                steps{
                                        sh 'chmod +x gradlew'
                                        sh './gradlew :spotlessApply'
                                        sh './gradlew clean build'
                                        sh './gradlew spotlessCheck'
                                }
                        }
                }
                post {
                        always {
                                archiveArtifacts artifacts: 'build/libs/*.jar', fingerprint: true
                        }
                }
        }

}
